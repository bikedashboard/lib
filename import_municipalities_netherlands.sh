wget -O cbs_data.zip https://www.cbs.nl/-/media/cbs/dossiers/nederland%20regionaal/wijk-en-buurtstatistieken/2018/shape%202018%20versie%2010.zip
unzip cbs_data.zip -d cbs_data

cd cbs_data/Uitvoer_shape

echo 'Import gemeentes (municipalities) CBS'
shp2pgsql -s 28992:4326 gem_2018.shp public.municipalities | psql deelfietsdashboard
echo 'Import wijken (areas) CBS'
shp2pgsql -s 28992:4326 wijk_2018.shp public.areas | psql deelfietsdashboard
echo 'Import buurten (neighboorhoods) CBS'
shp2pgsql -s 28992:4326 buurt2018.shp public.neighborhoods | psql deelfietsdashboard

# Add shapes from municipalities, residential_areas en neighborhoods to zones.
psql deelfietsdashboard -f insert_in_zones.sql
