import psycopg2
import inquirer
import requests
import random
import string
import os
import json
import sys

if os.getenv("APIKEY") == None:
    print("No APIKEY is specified.")
    sys.exit()

headers = {
    'Authorization': os.getenv("APIKEY"),
    'Content-Type': 'application/json'
}

questions = [
  inquirer.Text('email', message="What's the email username?"),
  inquirer.List('user_type',
                message="What type of user do you want to make??",
                choices=['administer', 'municipality', 'operator'],
  ),
  inquirer.Text('municipality_filter', message="Filter on municipality (when no data is entered no filter is made, if you want to specify multiple municipalities seperate them with a ,) "),
  inquirer.Text('operator_filter', message="Filter on operator (when no data is entered no filter is made, if you want to specify multiple operators seperate them with a ,) ")
]

answers = inquirer.prompt(questions)
print(answers)

def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

password = random_string_generator(12)

create_user_data = {}
create_user_data["user"] = {}
create_user_data["user"]["username"] = answers["email"]
create_user_data["user"]["email"] = answers["email"]
create_user_data["user"]["password"] = password
print(json.dumps(create_user_data))

r = requests.post("https://auth.deelfietsdashboard.nl/api/user", headers=headers, data=json.dumps(create_user_data))
if r.status_code != 200:
    print("Something went wrong")
    print(r.content)
    print(r.status_code)
response_user = r.json()

assign_application = {}
assign_application["registration"] = {}
assign_application["registration"]["applicationId"] = "bf901170-a2db-4f91-8ca7-24921e961193" 
assign_application["registration"]["roles"] = [answers["user_type"]]

r = requests.post("https://auth.deelfietsdashboard.nl/api/user/registration/" + response_user["user"]["id"], headers=headers, data=json.dumps(assign_application))
if r.status_code == 200:
    print("Succesfully created user in Fusionauth!")

conn = psycopg2.connect("dbname=deelfietsdashboard")
cur = conn.cursor()

username = answers["email"]
filter_municipality = (answers["municipality_filter"] != "") 
filter_operator = (answers["operator_filter"] != "")
is_admin = (answers["user_type"] == "administer")
cur.execute("INSERT INTO acl VALUES (%s, %s, %s, %s)", (username, filter_municipality, filter_operator, is_admin))

if filter_municipality:
    for gm_code in answers["municipality_filter"].split(","):
        cur.execute("INSERT INTO acl_municipalities VALUES (%s, %s)", (username, gm_code))

if filter_operator:
    for operator in answers["operator_filter"].split(","):
        cur.execute("INSERT INTO acl_operator VALUES (%s, %s)", (username, operator))

conn.commit()
cur.close()
print("Succesfully created user %s with password %s" % (answers["email"], password))
