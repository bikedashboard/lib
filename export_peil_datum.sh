# Dit script is bedoeld om te helpen bij het maken van een export op de peildatum.
# Het laat per gemeente zien welke accounts er actief zijn
# en per gemeente het aantal voertuigen wat actief is.


psql -d deelfietsdashboard -c "COPY (SELECT name as gemeente, users as gebruikers
FROM
(select municipality, string_agg(username, ' ') as users
FROM acl_municipalities 
GROUP BY municipality) as q1
LEFT JOIN zones
ON q1.municipality = zones.municipality
WHERE zone_type = 'municipality')
TO STDOUT WITH CSV HEADER" > actieve_accounts_per_gemeente.csv


psql -d deelfietsdashboard -c "COPY (SELECT name as gemeente, coalesce(value, 0) as aantal_voertuigen
FROM 
zones
LEFT JOIN (
    SELECT * 
    FROM stats_pre_process 
    WHERE date = '2021-07-01' 
    and stat_description = 'number_of_vehicles_available' 
    and system_id is null
) as q1
ON CONCAT('cbs:',municipality) = zone_ref
WHERE zone_type = 'municipality'
ORDER BY aantal_voertuigen DESC) TO STDOUT WITH CSV HEADER" > aantal_voertuigen_per_gemeente_peildatum.csv



